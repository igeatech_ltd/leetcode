#pragma once

#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

vector<vector<int>> threeSum(vector<int>& nums) {
	

	vector<vector<int>> results;

	sort(nums.begin(), nums.end());

	
	int right = nums.size() - 1;

	for (unsigned int i = 0; i < nums.size(); i++)
	{
		int left = i;
		int middle = i + 1;



		while (middle < right)
		{
			int sum = nums[i] + nums[middle] + nums[right];
			if (sum == 0)
			{
				vector<int> newVector = { nums[i], nums[middle], nums[right] };
				results.push_back(newVector);
				right--;
				middle++;

				int j = nums[middle];
				int k = nums[left];
				while (j == nums[middle])
				{
					middle++;
				}
				while (k == nums[right])
				{
					right--;
				}
			}
			else
			{

				if (sum > 0)
				{
					right--;
				}
				else if(sum < 0)
				{
					middle++;
				}



			}
		}
		

	}

	return results;
}
