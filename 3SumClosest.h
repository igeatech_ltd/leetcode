#pragma once

#include<vector>
#include<algorithm>
#include<iostream>

using namespace std;

int threeSumClosest(vector<int>& nums, int target) {

	int numberSum = 0;
	int right = nums.size() - 1;
	int currentDelta = INT_MAX;
	

	int ret = nums[0] + nums[1] + nums[2];

	sort(nums.begin(), nums.end());

	int result = nums[0] + nums[1] + nums[2];
	for (unsigned int i = 0; i < nums.size(); i++)
	{
		int middle = i + 1;
		int right = nums.size() - 1;
		
		while (middle < right)
		{
	
			int tempSum = nums[i] + nums[middle] + nums[right];
			if (abs(tempSum - target) < (abs(result - target)))
			{
				result = tempSum;
				if (result == target) return result;
			}

			if (tempSum > target) right--;
			else
			{
				middle++;
			}

			

		}

	}

	return result;

}