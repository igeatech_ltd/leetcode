#pragma once

#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;


//N^2 Solution does not pass Leetcode tests completely

//int maxArea(vector<int> array)
//{
//
//
//	int maxArea=0;
//	int base=0;
//	int height=0;
//
//	for (unsigned int i = 0; i < array.size(); i++)
//	{
//
//		for (unsigned int j = 1; j < array.size(); j++)
//		{
//
//			//height calculation
//			if (array[i] > array[j])
//			{
//				height = array[j];
//			}
//			else
//			{
//				height = array[i];
//			}
//
//			//Base Calculation
//			base = j - i;
//
//			int maxTempArea = (base * height);
//
//			if (maxTempArea > maxArea)
//			{
//				maxArea = maxTempArea;
//			}
//
//		}
//
//
//	}
//
//
//	return maxArea;
//
//}


struct bar
{

	int height;
	int pos;

};

//Optimized Solution O(n)

//int maxArea(vector<int>height)
//{
//	
//			int maxArea = 0, left = 0, right = height.size() - 1;
//
//			while (left < right) {
//				int area = min(height[left], height[right]) * (right - left);
//				maxArea = max(maxArea, area);
//
//				// Smaller of the two moves inward
//				if (height[left] < height[right])
//					left++; // left smaller
//				else
//					right--; // right smaller or equal
//			}
//
//			return maxArea;
//	};

int maxArea(vector<int>height)
{


	unsigned int vectorSize = height.size();

	int left = 0;
	int right = vectorSize - 1;
	int maxArea = 0;

	for (unsigned int i = 0; i < vectorSize; i++)
	{

		int base = right - left;
		int he = 0;
		if (height[left] >= height[right])
		{
			int tmphe = height[left] - height[right];
			he = height[left] - tmphe;
		}
		else
		{
			int tmphe = height[right] - height[left];
			he = height[right] - tmphe;
		}

		int tempArea = base * he;

		maxArea = max(tempArea, maxArea);

		if (height[right] > height[left])
		{
			left++;
			
		}
		else
		{
			right--;
		}
		

	}

	return maxArea;
}

