// LeetCodeProblems.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>

#include "MaximumSubarray.h"
#include "MergeSort.h"
#include "Beautiful_Arrangment_II.h"
#include "Container_Most_Water.h"
#include "Min_Subarray_Length.h"
#include "3sum.h"
#include "3SumClosest.h"
#include "TaskScheduler.h"

using namespace std;

int maxSubArray(vector<int>& myVector)
{

	int max_so_far = INT_MIN;
	int max_ending_here = 0;

	for (unsigned int i = 0; i < myVector.size(); i++)
	{

		max_ending_here = max_ending_here + myVector[i];

		if (max_so_far < max_ending_here)
		{
			max_so_far = max_ending_here;
		}

		if (max_ending_here < 0)
		{

			max_ending_here = 0;

		}

	}

	return max_so_far;
}

string isBalanced(string s) {

	stack<char> charCollection;

	for (unsigned int i = 0; i < s.size(); i++)
	{

		if ((s[i] == '(') || (s[i] == '[') || (s[i] == '{')) {

			charCollection.push(s[i]);

		}
		else if ((s[i] == ')') || (s[i] == ']') || (s[i] == '}')) {

			if (charCollection.size() == 0) {

				return(string("NO"));

			}
			else {
				char cj = charCollection.top();
				charCollection.pop();
				if ((s[i] == ')') && (cj == '(') || ((s[i] == ']') && (cj == '[')) || ((s[i] == '}')) && (cj == '{'))
				{

				}
				else
				{
					return(string("NO"));
				}
			}
		}	
	}

	if (charCollection.size() == 0)
	{
		return(string("YES"));
	}
	return(string("NO"));
}


//int maxSubArraySum(int a[], int size)
//{
//	int max_so_far = INT_MIN, max_ending_here = 0;
//
//	for (int i = 0; i < size; i++)
//	{
//		max_ending_here = max_ending_here + a[i];
//		if (max_so_far < max_ending_here)
//			max_so_far = max_ending_here;
//
//		if (max_ending_here < 0)
//			max_ending_here = 0;
//	}
//	return max_so_far;
//}
	

int main()
{

	//vector<int> collection = {1,8,6,2,5,4,8,3,7};
	//vector<int>collection = { 2, 3, 4, 5, 18, 17, 6 };
	//vector<int>collection = {-1, 0, 1, 2, -1, -4};
	//vector<int>collection = { -2, 0, 0, 2, 2 };
	//vector<int>collection = { -1, 2, 1, -4};
	//vector<int>collection = { 0, 2, 1, -3 };
	//vector<int>collection = { 1, 1, 1, 0 };
	vector<char>collection = {'A', 'A', 'A', 'B', 'B', 'B'};
	
	int result = leastInterval(collection, 2);

	return 0;

	}


