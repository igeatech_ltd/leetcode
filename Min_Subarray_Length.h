#pragma once
#include <vector>
#include <algorithm>

using namespace std;

int minSubArrayLen(int s, vector<int>& nums) {

	for (unsigned int i = 0; i < nums.size(); i++)
	{
		if (nums[i] == s)
		{
			return 1;
		}
	}

	int startPointer = 0;
	int endPointer = nums.size() - 1;
	int min_distance = 0;
	int temp_sum = 0;

	sort(nums.begin(), nums.end());

	for (unsigned int i = 0; i < nums.size(); i++)
	{
		/*if (((startPointer + 1) > nums.size()) && (endPointer <= 0))
		{
			return min_distance;
		}*/
		temp_sum += nums[startPointer] + nums[endPointer];
		if (temp_sum == s)
		{
			int min_temp_distance = endPointer - startPointer;
			if (min_temp_distance > min_distance)
			{
				min_distance = min_temp_distance;
			}

			
			{
				startPointer++;
				endPointer--;
			}

			
		}
		else if (temp_sum < s)
		{
			startPointer++;

		}
		else
		{
			endPointer--;
		}
	}

	return min_distance;

}